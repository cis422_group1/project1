# CIS 422
# Project 1: Simple Calendar
#
# Created by: Sam Svindland
# Modified on: 4/29/2018

INVENV = . env/bin/activate ; export FLASK_DEBUG=1 ;

# runs the main program, requires that the programs was installed
run: install
	($(INVENV) python3 calendar/main.py)& ./node_modules/.bin/electron .

# runs the server by itself
server:
	($(INVENV) python3 calendar/main.py)

# runs the client by itself
client:
	./node_modules/.bin/electron .

# installs the server and client
install:
	npm install electron
	python3 -m venv env
	$(INVENV) pip3 install -r requirements.txt

# updates the reqirements
dist:	env
	$(INVENV) pip freeze > requirements.txt

# removes compiled code
clean:
	rm -f *.pyc */*.pyc
	rm -rf __pycache__ */__pycache__

# removes compiled and installed code
veryclean:
	make clean
	rm -rf env
	rm -rf .DS_Store
	rm -rf calendar/.DS_Store
	rm -rf node_modules
	rm -f cal.sqlite
	rm -f backup*.csv
