# Project1 - Calendar

## Installation
### Required
* python3
* node.js

## Running
```
npm install
```
Afterwards entering the following command into a terminal that has GNU make will install everything else required.
```
make
```
(To use the above command you must be in the directory where you have placed the calendars other files.)

##Usage
###Home button
  Click on the "Home" button at any time to return to the current month.
  Make sure to return to the home screen before following any of the instructions below as they assume that you are starting from the home page.

###Creating an Event
  Either click on the "Add Event" button at the top of the calendar or click on a day and then click on the "Add Event" button (The button will be in the same place on either page).
  Enter the information for the event.
  Press the "Submit" button.

###Deleting an Event
  Click on the day whose event you wish to delete.
  Find the event you wish to delete in the list of events for the day.
  Press the "delete" button next to the event you wish to delete.

###Editing an Event
  Click on the day whose event you wish to edit.
  Find the event you wish to change in the list of events for the day.
  Press the "edit" button next to the event you wish to edit.
  Enter the new information for the event.
  Press the "Submit" button.

###Viewing all entered Events
  Click on the "Database Menu" button at the top of the calendar.
  Click on the "Show Database" button at the top of the calendar.
  All created events should now be shown in chronological order.

###Backing up your Events
  Click on the "Database Menu" button at the top of the calendar.
  Click on the "Backup Database" button.

###Clearing all Events
  Click on the "Database Menu" button at the top of the calendar.
  Click on the "Clear Database" button.

###Viewing future Months
  The arrows to the left and right of the Month name at the top of the calendar may be used to view other months.
  The left hand arrow moves the calendar's view one month back in time.
  The right hand arrow moves the calendar's view one month forward in time.

###Getting help
  When on the home screen there is a help button at the top left of the screen. Clicking it will show the above directions for operating this app.

## Known Bugs
* When on non-current month, reloading causes a 405 error on electron. On browser requries form-resubmission

# Technical Details

# File Structure

```
project1
│   README.md
│   requirements.txt
|   Makefile   
│
└───calendar
│   │   dataHandler.py
│   │   eventLogic.py
│   │   main.js
│   │   main.py
│   │
│   └───templates
│   │     addEvent.html
│   │     backup.html
│   │     dayPage.html
│   │     dbase.html
│   │     dbaseMenu.html
│   │     eventPage.html
│   │     help.html
│   │     index.html
│   │
│   └───static
│         index.css
└───docs

```

# Technical Summary

* The GUI components consist of a css stylesheet in the static folder and HTML files in the templates folder. HTML is displayed
using the Electron app framework initialized with main.js.

* The backend was written in Python 3 and SQLite 3 with UI Bridge functionality provided by a Flask interface.

* The UI Bridge consists of Python 3 code in main.py implementing the Flask http interface to modify html and receive
data transfers from the GUI frontend.

* The Data Handler, specified in dataHandler.py, recieves requests from the Flask based UI Bridge and then handles database
requests via Event Logic code in eventLogic.py.

* The Event Logic source code in eventLogic.py processes requests to the sql based database using python's sqlite3 module.

* The Date Logic source code is in dateLogic.py. It is currently very basic but could be expanded if more functionality is needed.
Much of the Date Logic functionality is obtained using Python libraries such as arrow, datetime, and calendar.

# Authors
* Ethan Quick
* Andrew Cvitanovich
* Sam Svindland
* Justin Robles
