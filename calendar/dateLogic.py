'''
CIS 422
Project 1: Simple Calendar

Filename: dateLogic.py

Description: The "Date Logic" component of the app architecture. Helper
functions for processing datetime information. There is not much here now but
could be expanded later for more functionality.

Requires: python3

Created by: Sam Svindland, Andrew Cvitanovich, Justin Robles and Ethan Quick

Modified on dates: 4/15/2018 <-> 4/29/2018
'''


from datetime import datetime, time

# HELPER CLASS FOR DATE LOGIC
class dateLogic:
    def __init__(self):
        pass
    # RETURNS DAY OF WEEK FOR ISO-8601 DATETIME STRINGS
    # Monday is 0 and Sunday is 6
    def day_of_week(self, theDate):
        dt_obj = datetime.strptime(theDate, "%Y-%m-%d %I:%M:%S")
        return datetime.weekday(dt_obj)

# # TESTING
# def main():
#     d = dateLogic()
#     result = d.day_of_week("2018-04-22 01:00:00")
#     print(result)
#
# if __name__ == "__main__":
#     main()
