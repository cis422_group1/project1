/*
CIS 422
Project 1: Simple Calendar

Filename: main.js

Description: Part of the GUI. Electron app control. The function createWindow uses
Electron calls to build the gui. Flask based functions in main.py and html
templates in the templates folder.

Created by: Sam Svindland, Andrew Cvitanovich, Justin Robles and Ethan Quick

Modified on dates: 4/15/2018 <-> 4/29/2018
*/

const {app, BrowserWindow} = require('electron')
const path = require('path')
const url = require('url')

// will create the electron window upon begin called
function createWindow() {
  win = new BrowserWindow({width: 800, height: 600})

  win.loadURL('http://127.0.0.1:5000')
  //win.webContents.openDevTools()
  win.on('closed', function() {
    win = null
  })
}

// calls createWindow when app opens
app.on('ready', createWindow)
