'''
CIS 422
Project 1: Simple Calendar

Filename: dataHandler.py

Description: The "Data Handler" component of the app architecture. The class
dataHandler recieves requests from the Flask based UI Bridge
(defined in main.py) and then handles database requests via Event Logic code
in eventLogic.py.

Requires: python3, eventLogic.py, dateLogic.py

Created by: Sam Svindland, Andrew Cvitanovich, Justin Robles and Ethan Quick

Modified on dates: 4/15/2018 <-> 4/29/2018
'''

from eventLogic import eventLogic
from dateLogic import dateLogic
import json


# EXAMPLE JSON RECORD
#json_record_example = '{"username":"nobody", "start_time":"2000-01-01 12:00:00","end_time":"2000-01-01 13:00:00", "title":"My Meeting", "notes":"These are my notes!"}'


class dataHandler:

    # __init__
    # creates a new database if it doesn't exist
    def __init__(self):
        self._db = eventLogic();
        self._date = dateLogic();

    # Method: getDayOfWeek
    # Parameters: theDate (an iso-8601 formatted date)
    # Behavior: Returns an integer specifying which day of the week matches the date. (0=Monday, 6=Sunday)
    # Return Result: An integer for the day of the week
    def getDayOfWeek(self, theDate):
        return self._date.day_of_week(theDate)

    # Method: addRecord
    # Parameters: theRecord (a json object like the example above)
    # Behavior: Adds a new event specified by the JSON object theRecord
    # Return Result: nothing
    def addRecord(self, theRecord):
        result = self._db.insertRecord(theRecord)
        return result

    # Method: deleteRecord
    # Parameters: theRecord (a json object like the example above)
    # Behavior: Deletes anything matching the exact record in the database
    # Return Result: nothing
    def deleteRecord(self, theRecord):
        self._db.deleteRecord(theRecord)

    # Method: getEventsInDateRange
    # Parameters: beginDate, endDate (both are iso-8601 formatted dates)
    # Behavior: Takes beginDate="YYYY-MM-DD" and endDate="YYYY-MM-DD" and returns a list of records in this range
    # Return Result: eventList (a list of JSON records)
    def getEventsInDateRange(self, beginDate,endDate):
        beginDate += " 00:00:00"
        endDate += " 24:00:00"
        eventList = self._db.retrieveRecords(beginDate,endDate)
        return eventList

    # Method: countEventsInDateRange
    # Parameters: beginDate, endDate (both are iso-8601 formatted dates)
    # Behavior: Takes beginDate="YYYY-MM-DD" and endDate="YYYY-MM-DD" and returns a list of records in this range
    # Return Result: integer count of matching events
    def countEventsInDateRange(self, beginDate,endDate):
        beginDate += " 00:00:00"
        endDate += " 24:00:00"
        count = self._db.countRecords(beginDate,endDate)
        return count

    # Method: backupUsersCalendar
    # Parameters: username (name of user who needs backup, a string)
    # Behavior: Finds all event records for a user and
    # Return Result: path to backup file
    def backupUsersCalendar(self):
        filePath = self._db.backupRecordsByUser()
        return filePath

    # Method: getAllEventsOrderedByDate
    # Parameters: username (name of user who needs backup, a string)
    # Behavior: Returns a list of all events ordered by date from earliest date to latest date
    # Return Result: Chronologically ordered list of events
    def getAllEventsOrderedByDate(self):
        return self._db.getRecordsChronologicalOrder()

    # Method: clearDatabase
    # Parameters: username
    # Behavior: Clears all event records from the database for specified user
    # Return Result: none
    def clearDatabase(self):
        self._db.clearDatabase()

# # TESTING
# #
# record0 = '{"username":"Andrew", "start_time":"2001-03-11 07:00:00", "end_time":"2001-03-11 13:00:00", "title":"Soccer Game", "notes":"This is Sparta!"}'
# #
# record1 = '{"username":"Fred", "start_time":"2005-03-12 07:00:00", "end_time":"2005-05-01 07:00:00", "title":"Vogon Poetry Contest", "notes":"Ode to a small lump of green putty."}'
# #
# record2 = '{"username":"Sadie", "start_time":"2015-03-20 08:00:00", "end_time":"2015-03-20 09:00:00", "title":"Who are you?", "notes":"Twas brillig and the slithy toves..."}'
#
# def main():
#     dh = dataHandler()
#     record0str = json.loads(record0)
#     record1str = json.loads(record1)
#     record2str = json.loads(record2)
#
#     print('Days of the week:')
#     print( dh.getDayOfWeek( record0str["start_time"] ) )
#     print( dh.getDayOfWeek( record1str["start_time"] ) )
#     print( dh.getDayOfWeek( record2str["start_time"] ) )
#
#     print('Inserting...')
#     dh.addRecord(record0str)
#     dh.addRecord(record1str)
#     dh.addRecord(record2str)
#
#     print('Retrieving...')
#     list = dh.getEventsInDateRange("1990-01-01","3000-01-01")
#     for r in list:
#         print(r)
#
#     print('Count: ', dh.countEventsInDateRange("1990-01-01","3000-01-01"))
#
#     print('Retrieve in chronological order:')
#     list = dh.getAllEventsOrderedByDate()
#     for r in list:
#         print(r)
#
#     print('Deleting...')
#     dh.deleteRecord(record0str)
#
#     print('Retrieving...')
#     list = dh.getEventsInDateRange("1990-01-01","3000-01-01")
#     for r in list:
#         print(r)
#
#     print('Count: ', dh.countEventsInDateRange("1990-01-01","3000-01-01"))
#
#     print('Backup...')
#     fname = dh.backupUsersCalendar()
#
#     print('Clear Database')
#     dh.clearDatabase()
#
#     print('Retrieve in chronological order:')
#     list = dh.getAllEventsOrderedByDate()
#     for r in list:
#         print(r)
#
#
# if __name__ == "__main__":
#     main()
