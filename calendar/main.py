'''
CIS 422
Project 1: Simple Calendar

Filename: main.py

Description: This is the main code for the "UI Bridge". Flask is used to handle
http requests from the Electron app. The modules calendar, time, arrow are used
to process date and time information. User requests are handled by the functions
below and any database requests are passed to the dataHandler module, while
necessary modifications to the gui are handled by the Flask based functions
below with the help of the html templates in the templates folder.

Created by: Sam Svindland, Andrew Cvitanovich, Justin Robles and Ethan Quick

Modified on dates: 4/15/2018 <-> 4/29/2018
'''

from __future__ import print_function
import time
from flask import Flask, render_template, request, url_for
import calendar
import datetime
import arrow
import json
from dataHandler import dataHandler

app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
# INITIALIZE THE DATA HANDLER
DH = dataHandler()
result = 0 # for error handling

# INITIALIZE FLASK ROUTINES
@app.route("/")
def hello():
    app.logger.debug("Main page entry")
    # Gets current day
    now = arrow.now()
    # startDay will be an int 0-6 representing monday-sunday numDays will be number of days in a month
    startDay, numDays = calendar.monthrange(now.year, now.month)


    # get count of events for each day:
    mo = "0{}".format(now.month)
    eventCount = [0] * numDays
    for day in range(1,numDays+1):
        if int(day) < 10:
            d = '0' + str(day)
        else:
            d = str(day)
        datestr = "{}-{}-{}".format(now.year, mo, d);
        eventCount[day-1] = DH.countEventsInDateRange(datestr,datestr)

    # I add one to startday since our calendar starts on sundays
    data = {"numDays": numDays,
            "today": now.day,
            "startDay": startDay % 6,
            "monthNum": now.month,
            "month": calendar.month_name[now.month],
            "year": now.year,
            "numEvents": eventCount}
    return render_template("index.html", data = data)

# CHANGE MONTH DISPLAYED
@app.route("/changeMonth", methods=["POST"])
def changeMonth():
    # Get information from the current pages form
    change = request.form['change']
    month, year = request.form['month'].split()
    if int(month) < 10:
        # The month needs to be two characters and not just "4"
        month = "0{}".format(month)
    # newMonth starts as the current month and year of the page the user was on
    # actualDate is todays date
    newMonth = arrow.get("{}-{}-01".format(year, month), "YYYY-MM-DD")
    actualDate = arrow.now()

    if change == '-1':
        # This means that the user wants to go to the previous month
        newMonth = newMonth.replace(months=-1)
    else:
        # Else go forward
        newMonth = newMonth.replace(months=+1)

    if newMonth.month == actualDate.month and newMonth.year == actualDate.year:
        # If the new month is the current month then set the current date to be highlighted
        today = actualDate.day
    else:
        # else set date out of range
        today = 32

    # startDay will be an int 0-6 representing monday-sunday numDays will be number of days in a month
    startDay, numDays = calendar.monthrange(newMonth.year, newMonth.month)

    # get count of events for each day:
    eventCount = [0] * numDays
    mo = "0{}".format(newMonth.month)
    for day in range(1,numDays+1):
        if int(day) < 10:
            d = '0' + str(day)
        else:
            d = str(day)
        datestr = "{}-{}-{}".format(year, mo, d);
        eventCount[day-1] = DH.countEventsInDateRange(datestr,datestr)

    # data is an object passed to javascript to fill in the page with the following information
    # Start day is incremented by 1 because it is in the range(0-6)
    data = {"numDays": numDays,
            "today": today,
            "startDay": startDay % 6,
            "monthNum": newMonth.month,
            "month": calendar.month_name[newMonth.month],
            "year": newMonth.year,
            "numEvents": eventCount}

    return render_template("index.html", data=data)

# CHANGE DAY DISPLAYED
@app.route("/changeToDay", methods=["POST"])
def changeToDay():
    # This is used to change from the month page into a single days page
    day = request.form['TheDay']
    month, year = request.form['month'].split()
    # The string for the month and day can be a single number ex. '4' when we need it to be '04'
    if int(month) < 10:
        month = '0' + str(month)
    else:
        month = str(month)

    if int(day) < 10:
        day = '0' + str(day)
    else:
        day = str(day)
    # Put together date string to be used to get events for that day
    date_str = str(year) + '-' + month + '-' + day

    # call to DB to grab events for the new day
    li = DH.getEventsInDateRange(date_str,date_str)
    output = [{"username": t[0], "starttime": t[1], "endtime": t[2], "title":t[3], "notes": t[4]} for t in li]

    data = {"monthNum": month,
            "day": day,
            "month": calendar.month_name[int(month)],
            "year": year}

    #output = [' '.join(t) for t in li]
    return render_template("dayPage.html", li=li, data=data)

# CHANGE TO THE NEXT DAY CHRONOLOGICALLY
@app.route("/nextDay", methods=["POST"])
def nextDay():
    #Used to change day when on a day page
    change = request.form['change']
    day, month, year = request.form['date'].split()
    # In this case I double check to see if the string isnt already in the correct format
    if int(day) < 10 and '0' not in month:
        # The month needs to be two characters and not just "4"
        day = "0{}".format(day)

    if int(month) < 10 and '0' not in month:
        # The month needs to be two characters and not just "4"
        month = "0{}".format(month)

    date = arrow.get("{}-{}-{}".format(year, month, day), "YYYY-MM-DD")

    if change == '-1':
        # This means that the user wants to go to the previous month
        date = date.replace(days=-1)
    else:
        # Else go forward
        date = date.replace(days=+1)

    # Date String
    if date.month < 10:
        mo = '0' + str(date.month)
    else:
        mo = str(date.month)

    if date.day < 10:
        d = '0' + str(date.day)
    else:
        d = str(date.day)

    date_str = str(date.year) + '-' + mo + '-' + d

    # call to DB to grab events for the new day
    li = DH.getEventsInDateRange(date_str,date_str)

    data = {"monthNum": date.month,
            "day": date.day,
            "month": calendar.month_name[date.month],
            "year": date.year}

    return render_template("dayPage.html", li=li, data=data)

# ADD AN EVENT
@app.route("/addEvent", methods=["POST"])
def addEvent():
    if len(request.form['date'].split()) == 3:
        # This means that this was called from a day page and will grab the day you are on
        day, month, year = request.form['date'].split()
        day = int(day)
        month = int(month)
        year = int(year)
    else:
        # else we use todays date
        now = arrow.now()
        year = now.year
        month = now.month
        day = now.day

    # Date String
    if month < 10:
        mo = '0' + str(month)
    else:
        mo = str(month)

    if day < 10:
        day = '0' + str(day)
    else:
        day = str(day)

    date_str = str(year) + '-' + mo + '-' + day

    return render_template("addEvent.html", result=result, date_str=date_str)

# SUBMIT NEW EVENT TO DATA HANDLER
@app.route("/newEvent", methods=["POST"])
def newEvent():
    event = request.form
    # parse event data
    start = arrow.get("{} {}".format(event["start_date"], event["start_time"]), 'YYYY-MM-DD HH:mm')
    end = arrow.get("{} {}".format(event["end_date"], event["end_time"]), 'YYYY-MM-DD HH:mm')
    if start > end:
        # If this is true then that means it is a valid entry
        return render_template("addEvent.html", result=-1, date_str=event["start_date"])

    eventToAdd = {
                    "username": event["username"],
                    "start_time": "{} {}".format(event["start_date"], event["start_time"]),
                    "end_time": "{} {}".format(event["end_date"],event["end_time"]),
                    "title": event['title'],
                    "notes": event['notes']
                 }

    # save event to the database
    result = DH.addRecord(eventToAdd)
    if result == 0:
        # If result == 0 then the record was added
        day, month, year = start.day, start.month, start.year
        if month < 10:
            month = '0' + str(month)
        else:
            month = str(month)

        if day < 10:
            day = '0' + str(day)
        else:
            day = str(day)
        li = DH.getEventsInDateRange(event["start_date"], event["start_date"])
        data = {"monthNum": month,
                "day": day,
                "month": calendar.month_name[int(month)],
                "year": year}
        return render_template("dayPage.html", li=li, data=data)
    else:
        # Else entry has failed
        return render_template("addEvent.html", result=result, date_str=event["start_date"])

# DELETE AN EVENT
@app.route("/deleteEvent", methods=['POST'])
def deleteEvent():
    # This part is a little hacky but it was the best solution I could come up with quickly without having to
    # change too much code
    # We we having trouble finding where a title or note ended when it used spaces.
    # What I did was added in ':NOTES:' in between the title and notes.
    # So this splits the event details then joins the title and notes and splits it at the :NOTES:
    event = request.form['delete'].split()
    title, notes = ' '.join(event[5:]).split(":NOTES:")
    eventToDel = {
        "username": event[0],
        "start_time": "{} {}".format(event[1], event[2]),
        "end_time": "{} {}".format(event[3], event[4]),
        "title": title,
        "notes": notes
    }
    # Delete record
    DH.deleteRecord(eventToDel)
    day, month, year = request.form['date'].split()
    date_str = str(year) + '-' + str(month) + '-' + str(day)

    li = DH.getEventsInDateRange(date_str, date_str)

    output = [{"username": t[0], "starttime": t[1], "endtime": t[2], "title": t[3], "notes": t[4]} for t in li]

    data = {"monthNum": month,
            "day": day,
            "month": calendar.month_name[int(month)],
            "year": year}

    return render_template("dayPage.html", li=li, data=data)

# DELETE AN EVENT FROM THE DATABASE (ALTERNATE VERSION)
@app.route("/deleteEvent2", methods=['POST'])
def deleteEvent2():
    event = request.form['delete'].split()
    title, notes = ' '.join(event[5:]).split(":NOTES:")
    eventToDel = {
        "username": event[0],
        "start_time": "{} {}".format(event[1], event[2]),
        "end_time": "{} {}".format(event[3], event[4]),
        "title": title,
        "notes": notes
    }
    DH.deleteRecord(eventToDel)

    li = DH.getAllEventsOrderedByDate()
    #output = [' '.join(t) for t in li]

    return render_template('dbase.html',li=li)

# VIEW DATABASE
@app.route("/dbase", methods=["GET", "POST"])
def dbase():

    li = DH.getAllEventsOrderedByDate()
    for i in range(len(li)):
        # This is used so we can have the current date information in the form to use to
        # go to the correct day page after submitting
        date = li[i][1].split("-")
        li[i] += (date[2].split()[0], date[1], date[0])

    return render_template('dbase.html',li=li)

# BACKUP DATABASE
@app.route("/backup")
def backup():
    filePath = DH.backupUsersCalendar()
    return render_template('backup.html',output=filePath)

# EMPTY CONTENTS OF THE DATABASE
@app.route("/clearDB")
def clearDB():
    DH.clearDatabase()
    return render_template('dbase.html',output='EMPTY DATABASE')

# GET HELP FOR USER
@app.route("/help")
def help():
    return render_template('help.html')

# VIEW THE DATABASE MENU OPTIONS
@app.route("/dbMenu")
def dbMenu():
    return render_template('dbaseMenu.html')

@app.route("/changeToEdit",  methods=["POST"])
def changeToEdit():
    day, month, year = request.form['date'].split()
    event = request.form['edit'].split()
    title, notes = ' '.join(event[5:]).split(":NOTES:")
    event = {
        "username": event[0],
        "startDate": event[1],
        "startTime": event[2],
        "endDate": event[3],
        "endTime": event[4],
        "title": title,
        "notes": notes,
        "day": day,
        "month": month,
        "year": year
    }

    return render_template("editEvent.html", result=0, data=event)

@app.route("/editEvent",  methods=["POST"])
def editEvent():
    # Used to edit events already in database
    # Get the info
    event = request.form['old'].split()
    title, notes = ' '.join(event[5:]).split(":NOTES:")
    day, month, year = request.form['date'].split()
    oldEvent = {
        "username": event[0],
        "start_time": "{} {}".format(event[1], event[2]),
        "end_time": "{} {}".format(event[3], event[4]),
        "title": title,
        "notes": notes
    }
    # make the arrow objects to use to compare
    start = arrow.get("{} {}".format(request.form['start_date'], request.form['start_time'], 'YYYY-MM-DD HH:mm'))
    end = arrow.get("{} {}".format(request.form['end_date'], request.form['end_time']), 'YYYY-MM-DD HH:mm')

    if start <= end:
        # if this is true then it is valid
        newEvent = {
            "username": request.form['username'],
            "start_time": "{} {}".format(request.form['start_date'], request.form['start_time']),
            "end_time": "{} {}".format(request.form['end_date'], request.form['end_time']),
            "title": request.form['title'],
            "notes": request.form['notes']
        }

        result = DH.addRecord(newEvent)
        if result == 0:
            # This means that the record was added
            DH.deleteRecord(oldEvent)
            date_str = newEvent["start_time"].split()[0]
            li = DH.getEventsInDateRange(date_str, date_str)

            data = {"monthNum": month,
                    "day": day,
                    "month": month,
                    "year": year}

            if type(month) == str:
                data['month'] = calendar.month_name[int(month)]
            # if successful then it goes to the daypage
            return render_template("dayPage.html", li=li, data=data)

    # Else add in extra info to redisplay the edit event page
    oldEvent['startTime'] = oldEvent["start_time"].split()[1]
    oldEvent['startDate'] = oldEvent["start_time"].split()[0]
    oldEvent['endTime'] = oldEvent["end_time"].split()[1]
    oldEvent['endDate'] = oldEvent["end_time"].split()[0]
    oldEvent["day"]= day
    oldEvent["month"]= month
    oldEvent["year"]= year
    return render_template("editEvent.html", result=-1, data=oldEvent)



if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000)
