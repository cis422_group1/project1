'''
CIS 422
Project 1: Simple Calendar

Filename: eventLogic.py

Description: The "Event Logic" component. Processes requests to the sql based
database using python's sqlite3 module.

Requires: sqlite3, python3

Created by: Sam Svindland, Andrew Cvitanovich, Justin Robles and Ethan Quick

Modified on dates: 4/15/2018 <-> 4/29/2018
'''

import sqlite3
import json
from datetime import datetime, time
import csv
import os.path

dbfile = 'cal.sqlite'

# DATETIME FORMAT
# ISO-8601
# YYYY-MM-DD HH:MM:SS

# EXAMPLE JSON RECORD
#json_record_example = '{"username":"nobody", "start_time":"2000-01-01 12:00:00","end_time":"2000-01-01 13:00:00", "title":"My Meeting", "notes":"These are my notes!"}'


# DATABASE HELPER CLASS
class eventLogic:
    def __init__(self):
        # CONNECT TO DATABASE
        conn = sqlite3.connect(dbfile)
        c = conn.cursor()
        # CREATE NEW TABLE IF IT HAS NOT BEEN CREATED BEFORE
        c.execute('CREATE TABLE IF NOT EXISTS events (username text, start_time text, end_time text, title text, notes text, CONSTRAINT no_duplicates UNIQUE (username,start_time,end_time,title,notes))')
        # COMMIT AND CLOSE
        conn.commit()
        conn.close()

    # INSERT A RECORD
    # theRecord must be a JSON string
    def insertRecord(self, theRecord):
        # CONNECT TO DATABASE
        conn = sqlite3.connect(dbfile)
        c = conn.cursor()

        try:
            # INSERT THE RECORD
            record =  tuple( list( theRecord.values() ) )

            c.execute('INSERT INTO events(username, start_time, end_time, title, notes) VALUES(?,?,?,?,?)',record)
            # COMMIT AND CLOSE
            conn.commit()
            conn.close()
            return 0
        except sqlite3.IntegrityError as e:
            print("Warning: This event is already in the database")
            return -1
        except:
            print("Error: Cannot insert record.")
            return -1



    # DELETE A RECORD
    # theRecord must be a JSON string
    def deleteRecord(self, theRecord):
        # CONNECT TO DATABASE
        conn = sqlite3.connect(dbfile)
        c = conn.cursor()

        # DELETE THE RECORD
        record =  tuple( list( theRecord.values() ) )
        c.execute('DELETE FROM events WHERE (username=? AND start_time=? AND end_time=? AND title=? AND notes=?)', record)

        # COMMIT AND CLOSE
        conn.commit()
        conn.close()

    # RETRIEVE RECORDS OCCURING IN RANGE GIVEN BY startTime AND endTime
    # startTime AND endTime MUST BE ISO-8601 FORMATTED DATETIME STRINGS
    def retrieveRecords(self, startTime, endTime ):
        # CONNECT TO DATABASE
        conn = sqlite3.connect(dbfile)
        c = conn.cursor()

        # SELECT THE RECORD (TODO: FIX SQL CALL HERE)
        c.execute(
        '''SELECT * FROM events WHERE
        ( DATE(?) BETWEEN DATE(start_time) AND DATE(end_time) )
        OR ( DATE(?) BETWEEN DATE(start_time) AND DATE(end_time) )
        OR ( DATE(start_time) BETWEEN DATE(?) AND DATE(?) )
        OR ( DATE(end_time) BETWEEN DATE(?) AND DATE(?) )'''
        , (startTime, endTime, startTime, endTime, startTime, endTime))

        # GET LIST OF RECORDS
        theData = c.fetchall()

        # CLOSE DATABASE
        conn.close()

        # RETURN LIST OF RECORDS
        return theData


    # COUNT RECORDS OCCURING IN RANGE GIVEN BY startTime AND endTime
    # startTime AND endTime MUST BE ISO-8601 FORMATTED DATETIME STRINGS
    def countRecords(self, startTime, endTime ):
        # CONNECT TO DATABASE
        conn = sqlite3.connect(dbfile)
        c = conn.cursor()

        # SELECT THE RECORD (TODO: FIX SQL CALL HERE)
        c.execute(
        '''SELECT COUNT(*) FROM events WHERE
        ( DATE(?) BETWEEN DATE(start_time) AND DATE(end_time) )
        OR ( DATE(?) BETWEEN DATE(start_time) AND DATE(end_time) )
        OR ( DATE(start_time) BETWEEN DATE(?) AND DATE(?) )
        OR ( DATE(end_time) BETWEEN DATE(?) AND DATE(?) )'''
        , (startTime, endTime, startTime, endTime, startTime, endTime))

        # GET LIST OF RECORDS
        cnt = c.fetchone()

        # CLOSE DATABASE
        conn.close()

        # RETURN NUMBER OR RECORDS
        return cnt[0]

    # CREATE BACKUP FILE
    # theBackupFile IS THE STRING FOR BACKUP FILE NAME
    # uname IS THE USERNAME OF DESIRED RECORDS
    # returns path of backup file
    def backupRecordsByUser(self):

        # get current date
        now = datetime.now()

        # create filename string
        fname = "backup-"
        fname += str(now.year)
        fname += "-"
        fname += str(now.month)
        fname += "-"
        fname += str(now.day)
        fname += ".csv"

        # CONNECT TO DATABASE
        conn = sqlite3.connect(dbfile)
        c = conn.cursor()

        # SELECT RECORDS
        c.execute('SELECT * FROM events')

        # GET LIST OF RECORDS
        theData = c.fetchall()

        # CLOSE DATABASE
        conn.close()

        # OPEN CSV FILE AND WRITE DATA TO BACKUP
        f = open(fname,'w')
        file_path = os.path.realpath(f.name)
        writer = csv.writer(f, delimiter='\t')
        writer.writerows(theData)
        return file_path

    # RETURN LIST OF ALL RECORDS IN DESCENDING ORDER BY DATE
    def getRecordsChronologicalOrder(self):
        # CONNECT TO DATABASE
        conn = sqlite3.connect(dbfile)
        c = conn.cursor()

        # SELECT RECORDS
        c.execute('SELECT * FROM events ORDER BY DATETIME(start_time) ASC')

        # GET LIST OF RECORDS
        theData = c.fetchall()

        # CLOSE DATABASE
        conn.close()
        return theData

    # DELETE ALL RECORDS FROM DATA BASE FOR USER
    def clearDatabase(self):
        # CONNECT TO DATABASE
        conn = sqlite3.connect(dbfile)
        c = conn.cursor()

        # SELECT RECORDS
        c.execute('DELETE FROM events')

        # COMMIT AND CLOSE
        conn.commit()
        conn.close()
        return

# # EXAMPLE JSON RECORDS
# json_record_example = '{"username":"nobody", "start_time":"2000-01-01 12:00:00", "end_time":"2000-01-01 13:00:00", "title":"My Meeting", "notes":"These are my notes!"}'
# #
# record0 = '{"username":"Andrew", "start_time":"2001-03-11 07:00:00", "end_time":"2001-03-13 13:00:00", "title":"Soccer Game", "notes":"This is Sparta!"}'
# #
# record1 = '{"username":"Bob", "start_time":"2005-03-12 07:00:00", "end_time":"2005-05-01 07:00:00", "title":"Vogon Poetry Contest", "notes":"Ode to a small lump of green putty."}'
# #
# record2 = '{"username":"George", "start_time":"2015-03-20 08:00:00", "end_time":"2015-03-20 09:00:00", "title":"Who are you?", "notes":"Twas brillig and the slithy toves..."}'
#
# #TESTING ONLY
# def main():
#     db = eventLogic()
#     # insert records
#     record0str = json.loads(record0)
#     record1str = json.loads(record1)
#     record2str = json.loads(record2)
#     print('Inserting...')
#     db.insertRecord(record0str)
#     db.insertRecord(record1str)
#     db.insertRecord(record2str)
#     print('Retrieving...')
#     # retrieve records
#     list = db.retrieveRecords("1990-01-01 00:00:00","3000-04-29 00:00:00")
#     for r in list:
#         print(r)
#     print('Record count: ',db.countRecords("1990-01-01 00:00:00","3000-04-29 00:00:00"))
#     # test multiday record
#     print('Test multiday record...')
#     list = db.retrieveRecords("2001-03-12 00:00:00","2001-03-12 24:00:00")
#     for r in list:
#         print(r)
#     print('Record count: ',db.countRecords("2001-03-12 00:00:00","2001-03-12 24:00:00"))
#
#     print('Retrieve in chronological order:')
#     list = db.getRecordsChronologicalOrder()
#     for r in list:
#         print(r)
#     print('Deleting...')
#     db.deleteRecord(record0str)
#     print('Retrieving...')
#     list = db.retrieveRecords("1990-01-01 00:00:00","3000-01-01 24:00:00")
#     for r in list:
#         print(r)
#     print('Record count: ',db.countRecords("1990-01-01 00:00:00","3000-04-29 00:00:00"))
#     print('Backup...')
#     db.backupRecordsByUser()
#     print('Clearing database of all records...')
#     db.clearDatabase()
#     print('Retrieve in chronological order:')
#     list = db.getRecordsChronologicalOrder()
#
# if __name__ == "__main__":
#     main()
